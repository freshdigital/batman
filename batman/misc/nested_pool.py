# coding: utf8
"""NestedPool class.

This class is used when nested process pool are needed.
It modify the ``daemon`` attribute to allow this subprocessing.

"""
import pathos
from pathos.multiprocessing import ProcessPool
import multiprocess


class NoDaemonProcess(multiprocess.Process):
    """NoDaemonProcess class.

    Inherit from :class:`multiprocessing.Process`.
    The ``daemon`` attribute always returns False.
    """

    @property
    def daemon(self):
        """Return False."""
        return False

    @daemon.setter
    def daemon(self, val):

        pass


class NestedPool(pathos.multiprocessing.Pool):
    """NestedPool class.

    Inherit from :class:`pathos.multiprocessing.Pool`.
    Enable nested process pool.
    """

    def Process(self, *args, **kwds):
        proc = super(NestedPool, self).Process(*args, **kwds)
        proc.__class__ = NoDaemonProcess

        return proc


if __name__ == '__main__':
    p = NestedPool()
    x = ProcessPool().map(lambda x: x, p.map(lambda x: x, range(4)))
    print(x)
